#!/usr/bin/env bash


source /cvmfs/compass-mc.cern.ch/sw/corals/coral_git_2021-08-20gcc6.3/environment.sh 
source /cvmfs/compass-mc.cern.ch/sw/corals/coral_git_2021-08-20gcc6.3/install/setup.sh 

phastDIR=/afs/cern.ch/user/a/arivanov/workspace/AMBER/soft/phast/phast.8.022
cp /afs/cern.ch/user/a/arivanov/workspace/AMBER/analysis/runtgeant/options/shield/phast/UserEvent22.cc ${phastDIR}/user/UserEvent22.cc
cd 


make 