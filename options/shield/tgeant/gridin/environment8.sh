#!/bin/sh
 export NOW=2020-09-07
#
 export ROOTSYS=/cvmfs/compass-mc.cern.ch/sw/roots/root_v6.20.04_gcc8.3.0-opt
# GCC
 source /cvmfs/compass-mc.cern.ch/sw/x86_64-centos7-gcc8-opt/setup.sh
# ROOT
 source ${ROOTSYS}/bin/thisroot.sh 
# XROOTD (root buildin)
 source ${ROOTSYS}/bin/setxrd.sh  ${ROOTSYS}
 export XROOTD_DIR=${ROOTSYS}
#
 export ROOT_INCLUDE_PATH=$ROOTSYS/include 
# GEANT  
 export GEANT4_DIR=/cvmfs/sft.cern.ch/lcg/releases/Geant4/10.06.p01-014ca/x86_64-centos7-gcc8-opt
 cd ${GEANT4_DIR}/bin
 source ./geant4.sh 
 cd -
# Pythia 6
 export PYTHIA6=/cvmfs/compass-mc.cern.ch/sw/pythia/pythia6.28_gcc6.3.1
 export PYTHIA6_LIB=$PYTHIA6/lib
 export PYTHIA6_LIBRARY=$PYTHIA6/lib
 export PYTHIA6_DIR=$PYTHIA6 
# Pythya 8
 export PYTHIA8=/cvmfs/compass-mc.cern.ch/sw/pythia/pythia8244_gcc8.3.0
# LHAPDF
 export LHAPDF=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/5.9.1-76a09/x86_64-centos7-gcc62-opt
 export LHAPDF_DIR=$LHAPDF
 export LHAPATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/5.9.1/share/lhapdf/PDFsets
 export PYTHIA8_LIBRARY=$PYTHIA8/lib
 export PYTHIA8_DIR=$PYTHIA8 
#CERNLIB
 export PATH_CERN=/cvmfs/compass-mc.cern.ch/sw/cernlib/x86_64-centos7-linux_gcc8.3.0
 export YEAR_CL=2006   
 export CERN_LEVEL=2006
 export CERNLIB=${PATH_CERN}   
 export CERN=${CERNLIB}   
#MySQL
 export MYSQL=/cvmfs/sft.cern.ch/lcg/releases/mysql/5.7.20-d1585/x86_64-centos7-gcc8-opt
#Qt5 
# same as for GEANT4
 export QT5_HOME=/cvmfs/sft.cern.ch/lcg/releases/LCG_97/qt5/5.12.4/x86_64-centos7-gcc8-opt
 export Qt5_DIR=$QT5_HOME
 export QT_QPA_PLATFORM_PLUGIN_PATH=$QT5_HOME/plugins/platforms
 export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
#TGEANT
 export TGEANT=/afs/cern.ch/work/a/agridin/private/tgeant/TGEANT/install2/
 export DUMMY_TGEANT=$TGEANT   
 export BEAMFILES=/cvmfs/compass-mc.cern.ch/beamfiles
 export HEPGEN=$TGEANT/resources/hepgen
 export HEPGEN=$TGEANT

 export LD_LIBRARY_PATH=${PYTHIA6}:$LD_LIBRARY_PATH
 export LD_LIBRARY_PATH=${PYTHIA8}:$LD_LIBRARY_PATH
 export LD_LIBRARY_PATH=${LHAPDF}/lib:$LD_LIBRARY_PATH
 export LD_LIBRARY_PATH=$ROOTSYS/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/vdt/0.4.2-84b8c/x86_64-centos7-gcc63-opt/lib:${LD_LIBRARY_PATH}:
 export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/Davix/0.7.1-8d6d7/x86_64-centos7-gcc63-opt/lib64:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/tbb/2019_U1-5939b/x86_64-centos7-gcc63-opt/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/GSL/2.5-32fc5/x86_64-centos7-gcc63-opt/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_88/hdf5/1.8.18/x86_64-centos7-gcc62-opt/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=${PYTHIA6_LIBRARY}:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=${TGEANT}/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=$QT5_HOME/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=$MYSQL/lib:${LD_LIBRARY_PATH}
 export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/gdb/7.12.1-685c5/x86_64-centos7-gcc62-opt/lib:${LD_LIBRARY_PATH}

 export PATH=${GEANT4_DIR}/bin:${PATH}
 export PATH=${ROOTSYS}/bin:${PATH}
 export PATH=${TGEANT}/bin:${PATH}
 export PATH=${PATH_CERN}/${YEAR_CL}/bin:${PATH}
 export PATH=/cvmfs/sft.cern.ch/lcg/releases/gdb/7.12.1-685c5/x86_64-centos7-gcc62-opt/bin:${PATH}
 
export CPLUS_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:/cvmfs/compass-mc.cern.ch/sw/x86_64-centos7-gcc8-opt/include 
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:/cvmfs/sft.cern.ch/lcg/releases/vdt/0.4.2-84b8c/x86_64-centos7-gcc63-opt/include
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$LHAPDF/include
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$LHAPDF/include/LHAPDF
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$PYTHIA6_DIR/../include
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$PYTHIA8/include
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$GEANT4/include
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$GEANT4/include/Geant4
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:$HEPMC/include

export HEPGEN=$TGEANT
export PHAST=/cvmfs/compass-mc.cern.ch/sw/phasts/phast.8.014gcc8.3

export COMPASS_FILES=/cvmfs/compass-condb.cern.ch/detector
#cd /cvmfs/compass-mc.cern.ch/sw/corals/coral_git_2020-10-14gcc8.3/install
#source ./setup.sh
#cd -


#cmake3 -DCMAKE_INSTALL_PREFIX=$PWD/../TGEANT_INSTALL_$NOW/ ${PWD}/../TGEANT_FILES_$NOW/ 

#git clone http://github.com/root-project/root.git root_v6.21.01
#cmake3 -Dcxx17=ON -Dpythia6=ON -Dpythia8=ON -DCMAKE_INSTALL_PREFIX=$PWD/../root_v6.20.04_gcc8.3.0-opt -DGDML=ON -Dgdml:BOOL=ON -DCMAKE_CXX_FLAGS='-fPIC -O2' -Dminuit2=ON -Dsoversion=ON -Drpath=ON -Dgl=OFF -DCMAKE-BUILD-TYPE=Optimized -Dbuiltin_xrootd=ON -Dxrootd=ON $PWD/../root_v6.21.01 -Dbuiltin_glew=ON -Dbuiltin_afterimage=ON
# remember to copy built/bin/xr* to the root_installation_dir/bin 
