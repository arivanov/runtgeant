#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaAlgo.h"
#include "G3part.h"
#include "PaTPar.h"
#include "PaMetaDB.h"
#include "PaTrack.h"
#include <PaPid.h>
#include <algorithm>
#include <sstream>

using namespace std;

void UserEvent22(PaEvent &e)
{

      static int Run;
      static vector<string> NameDets;

      // ===============================================================
      static vector<TH2D *> h2hits;

      // ===============================================================
      static bool first(true);
      Run = e.RunNum();

      if (first)
      {
            // ===========  loop over detectros ========================
            int NDetectors = PaSetup::Ref().NDetectors();
            for (int ipl = 0; ipl < NDetectors; ipl++)
            {
                  const PaDetect &d = PaSetup::Ref().Detector(ipl);

                  cout << "det name=" << d.Name() << "  vZ=" << d.Z() << endl;
            }
            first = false;
      }

      for (int imc_t = 0; imc_t < e.NMCtrack(); imc_t++) // looping over number of MC tracks
      {
            const PaMCtrack &t = e.vMCtrk(imc_t); // assign MC track number

            const set<int> &vMChitRef = t.sMChitRef(); // MC hit references.

            cout << vMChitRef.size() << endl;
            for (auto imchit : vMChitRef)
            {

                  cout << imchit << endl;
            }

           // for (int imc_h = 0; imc_h < t.NMCHits(); imc_h++) // looping over number of MC hits
          //  {

          //        const PaMCtrack &t = e.vMCtrk(mc_t); // assign MC track number

          //  }
      }
}
