#!/usr/bin/env python3

'''
Script for run the jobs in HTcondor for SpdRoot
'''

import argparse
import os
import stat
import shutil
import yaml
import sys
import subprocess
import socket




def createParser():
    '''
    Read argv from command line
    '''

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", default="terminal",
                        choices=['terminal', 'htcondor'], type=str,
                        help="run jobs")
    parser.add_argument("-c", default="config.yaml",
                        type=str, help="file with configuration in YAML format: default file name is config.yaml")
    parser.add_argument("-t", action="store_false",
                        help="test mode: without running jobs")

    return parser


def loadConfiguration(namefile):
    '''
    Read configuration in YAML format
    '''
    if not os.path.exists(namefile):
        sys.exit(f"file {namefile} does not exist!")

    config = yaml.load(open(namefile), Loader=yaml.Loader)

    print('-'*50)
    for key in config:
        print(key, " : ", config[key])
    print('-'*50)

    return config


def createJobsDirectories(config):

    if os.path.exists(os.getcwd() + "/" + config.get("NAME")):
        user_input = input(
            f"Do you want to do clean {config.get('NAME')} ? (y/n)")

    for idir in ["logs", "subs", "output"]:

        name_sub_folder = os.getcwd() + "/" + config.get("NAME") + "/" + idir

        if not os.path.exists(name_sub_folder):
            os.makedirs(name_sub_folder)
        else:

            if (user_input == "y"):
                shutil.rmtree(name_sub_folder)
                os.makedirs(name_sub_folder)



def sendTerminalJobs(config, list_runs):

    path_to_run = os.getcwd() + "/" + config.get("NAME") + "/subs"

    for irun in list_runs:
        if args.t:

            path_to_file_bash = path_to_run + "/" + irun + "/" + irun + ".sh"
            print("run:", path_to_file_bash)
            subprocess.run(["bash", path_to_file_bash])



def sendHTcondorJobs(config, list_runs):

    path_to_file_htcondor = os.getcwd() + "/" + config.get("NAME")
    name_file_htcondor = path_to_file_htcondor + \
        "/" + config.get("NAME") + ".sub"

    file_htcondor = open(name_file_htcondor, 'w')
    file_htcondor.write("universe = vanilla\n")
    file_htcondor.write(
        f"executable = {path_to_file_htcondor}/subs/$(file)/$(file).sh\n")
    file_htcondor.write(
        f"error = {path_to_file_htcondor}/logs/$(file).err\n")
    file_htcondor.write(
        f"log = {path_to_file_htcondor}/logs/$(file).log\n")
    file_htcondor.write(
        f"output = {path_to_file_htcondor}/logs/$(file).out\n")
    file_htcondor.write("getenv = True\n")
    file_htcondor.write(f"BATCH_NAME = {config.get('NAME')}\n")

    file_htcondor.write(f"queue file from ( \n")
    for irun in list_jobs:
        file_htcondor.write(f"{irun}\n")
    file_htcondor.write(")")

    file_htcondor.close()

    os.chmod(name_file_htcondor,  0o777)

    if args.t:
        subprocess.run(["condor_submit", name_file_htcondor])

def createJobs(config, irun):
    name_folder_sub = os.getcwd() + "/" + \
        config.get("NAME") + "/subs/run" + str(irun)

    if not os.path.exists(name_folder_sub):
        os.mkdir(name_folder_sub)

    name_file_sub = name_folder_sub + "/run" + str(irun) + ".sh"
    file_sub = open(name_file_sub, 'w')
    file_sub.write("#!/usr/bin/env bash \n")

    file_sub.write(f"source  {os.getcwd()}/{config['CONFIG_ENV']} \n")
    
    print (name_folder_sub)

# ===== TGEANT
    if  config.get("TGEANT") is not None:
        path_options_tgeant=f"{config['OPTIONS']}/tgeant"
        
        shutil.copyfile(f"{path_options_tgeant}/{config['TGEANT']['SETTINGS']}",f"{name_folder_sub}/{config['TGEANT']['SETTINGS']}")

        modifyTGEANTsettings(config, f"{name_folder_sub}/{config['TGEANT']['SETTINGS']}", "run" + str(irun) )

        file_sub.write(f"export TGEANT={config['TGEANT']['INSTALL']} \n")
        file_sub.write("source $TGEANT/thisgeant.sh \n")
        file_sub.write(f"{config['TGEANT']['EXE']} {name_folder_sub}/run{str(irun)}{config['TGEANT']['SETTINGS']} \n")


# ===== CORAL
    if  config.get("CORAL") is not None:

        path_options_coral=f"{os.getcwd()}/{config['OPTIONS']}/coral"

        shutil.copyfile(f"{path_options_coral}/{config['CORAL']['SETTINGS']}",f"{name_folder_sub}/{config['CORAL']['SETTINGS']}")

        file_sub.write(f"source {config['CORAL']['INSTALL']}/environment.sh \n")
        file_sub.write(f"source {config['CORAL']['INSTALL']}/install/setup.sh \n")
        


        modifyCORALsettings(config, path_options_coral, f"{name_folder_sub}/{config['CORAL']['SETTINGS']}", name_folder_sub,  "run" + str(irun) )

        file_sub.write(f"{config['CORAL']['EXE']} {name_folder_sub}/run{str(irun)}{config['CORAL']['SETTINGS']} \n")


# ===== PHAST
    if  config.get("PHAST") is not None:
        path_options_phast=f"{os.getcwd()}/{config['OPTIONS']}/phast"

        run_dir = os.getcwd()
        #shutil.copyfile(f"{path_options_phast}/UserEvent{config['PHAST']['USEREVENT']}.cc",  f"{config['PHAST']['INSTALL']}/user/UserEvent{config['PHAST']['USEREVENT']}.cc")
        #os.chdir( config['PHAST']['INSTALL'])
        #subprocess.run(["make", "clean" ])
        #subprocess.run(["make", "-j4" ])
        #os.chdir(run_dir)

        file_sub.write(f"{config['PHAST']['EXE']} -u {config['PHAST']['USEREVENT']} -h  {name_folder_sub}/mDST.root  {name_folder_sub}/run{irun}mDST.root  \n")

        

    file_sub.write("\n")
    file_sub.close()

    os.chmod(name_file_sub, 0o777)

    return "run" + str(irun)




def modifyTGEANTsettings(config, file, run):
    
    fin = open(file, "rt")

    fout_name =os.path.dirname(file) + "/"+ run + os.path.basename(file)
    file_out = open(fout_name, "wt")
    

    for line in fin:
        flag_save=1
                   
        if "NEVENTS" in line:
            file_out.write(line.replace('NEVENTS', f"{config['NEVENTS']}"))    
            flag_save=0
        if "RUNNAME" in line:
            file_out.write(line.replace('RUNNAME', run))    
            flag_save=0
        
        if "OUPUT" in line:
            file_out.write(line.replace('OUPUT', os.path.dirname(file)))    
            flag_save=0
        if flag_save:
            file_out.write(line)



def modifyCORALsettings(config, path_options_coral, file, pathTo, run):
    
    fin = open(file, "rt")

    fout_name =os.path.dirname(file) + "/"+ run + os.path.basename(file)
    file_out = open(fout_name, "wt")
    


    for line in fin:
        file_out.write(line)
    
    file_out.write(f"CsTGEANTFile file {pathTo}/{run}_run000.tgeant \n")
    file_out.write(f"detector table {path_options_coral}/{config['CORAL']['DETDAT']} \n")
   # file_out.write(f"TraF Dicofit {path_options_coral}/{config['CORAL']['DICO']} \n")
    file_out.write(f"CsGDMLGeometry {path_options_coral}/{config['CORAL']['GDML']} \n")
    file_out.write(f"mDST file {pathTo}/{run}mDST.root  \n")
    file_out.write(f"TraF	Dicofit {path_options_coral}/{config['CORAL']['DICO']}  \n")





if __name__ == '__main__':

    parser = createParser()

    args = parser.parse_args()
    config = loadConfiguration(args.c)

    createJobsDirectories(config)

    Runmin = 0
    if (config.get("RUNMIN") is not None):
        Runmin = config.get("RUNMIN")

    Runmax = config.get("NRUNS")
    if (config.get("RUNMAX") is not None):
        Runmax = config.get("RUNMAX")

    list_jobs = []
    for irun in range(Runmin, Runmax):

        print(f"\t Run={irun}, NEvents={config.get('NEVENTS')}")
        list_jobs.append(createJobs(config, irun))

    if (args.r == "htcondor"):
        sendHTcondorJobs(config, list_jobs)

    if (args.r == "terminal"):
        sendTerminalJobs(config, list_jobs)
