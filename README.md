# run TGEANT+CORAL+PHAST

## Settings for TGEANT

It needs to make modification in file `settings.xml`

```xml
<outputPath>OUPUT</outputPath>
<runName>RUNNAME</runName>
<numParticles>NEVENTS</numParticles>
```
```xml
<exportGDML>false</exportGDML>
```

## Settings for COLAL

CsTGEANTFile file  - absolute path to your .tgeant file
detector table - absolute path to your detectors.dat file
TraF Dicofit  - absolute path to your dico file
CsGDMLGeometry  - file absolute path to your gdml file


###


## Settings for Rumen

/afs/cern.ch/user/a/arivanov/workspace/public/rumen/shield